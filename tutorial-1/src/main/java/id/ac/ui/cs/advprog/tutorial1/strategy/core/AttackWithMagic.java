package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public String attack() {
        return "I cast a spell on you";
    }

    public String getType() {
        return "Magic";
    }
}
