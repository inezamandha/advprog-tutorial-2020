package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    public String attack() {
        return "Dor! I attack you with gun";
    }

    public String getType() {
        return "Gun";
    }
}
