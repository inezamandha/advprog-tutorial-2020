package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public String defend() {
        return "I defend you with armor";
    }

    public String getType() {
        return "Armor";
    }
}
