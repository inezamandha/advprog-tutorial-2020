package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete Me
    String name;
    String role;
    List<Member> memberList;

    public PremiumMember (String name, String role) {
        this.name = name;
        this.role = role;
        this.memberList = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public String getRole() {
        return this.role;
    }

    public void addChildMember(Member member) {
        if(this.getRole().equals("Master")){
            this.memberList.add(member);
        } else {
            if(memberList.size()<3){
                this.memberList.add(member);
            }
        }

    }

    public void removeChildMember(Member member) {
        this.memberList.remove(member);
    }

    public List<Member> getChildMembers() {
        return this.memberList;
    }
}
