package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    //TODO: Complete Me
    String name;
    String role;
    List<Member> memberList;

    public OrdinaryMember (String name, String role) {
        this.name = name;
        this.role = role;
        this.memberList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public void addChildMember(Member member) {
        return;
    }

    public void removeChildMember(Member member) {
        return;
    }

    public List<Member> getChildMembers() {
        return new ArrayList<>();
    }
}
