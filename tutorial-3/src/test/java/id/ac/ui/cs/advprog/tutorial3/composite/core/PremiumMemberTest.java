package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member wati = new OrdinaryMember("Wati", "Gold Merchant");
        member.addChildMember(wati);
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member wati = new OrdinaryMember("Wati", "Gold Merchant");
        member.addChildMember(wati);
        member.removeChildMember(wati);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member wati = new OrdinaryMember("Wati", "Gold Merchant");
        Member wita = new OrdinaryMember("Wita", "Gold Merchant");
        Member witi = new OrdinaryMember("Witi", "Gold Merchant");
        Member witu = new OrdinaryMember("Witu", "Gold Merchant");
        member.addChildMember(wati);
        member.addChildMember(wita);
        member.addChildMember(witi);
        member.addChildMember(witu);
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        PremiumMember master = new PremiumMember("Inez", "Master");
        master.addChildMember(new OrdinaryMember("Bela","Murid"));
        master.addChildMember(new OrdinaryMember("Siti","Murid"));
        master.addChildMember(new OrdinaryMember("Dinda","Murid"));
        master.addChildMember(new OrdinaryMember("Thami","Murid"));
        assertEquals(4, master.getChildMembers().size());
    }
}
