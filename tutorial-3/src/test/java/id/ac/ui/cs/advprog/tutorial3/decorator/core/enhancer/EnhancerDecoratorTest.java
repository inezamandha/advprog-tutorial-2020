package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.WeaponProducer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){
        weapon1 = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
        weapon2 = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
        weapon3 = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
        weapon4 = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
        weapon5 = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();

        int value_weapon1 = weapon1.getWeaponValue();
        int value_weapon2 = weapon2.getWeaponValue();
        int value_weapon3 = weapon3.getWeaponValue();
        int value_weapon4 = weapon4.getWeaponValue();
        int value_weapon5 = weapon5.getWeaponValue();

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        //TODO: Complete me
        assertEquals(value_weapon1 + 55, weapon1.getWeaponValue());
        assertEquals(value_weapon2 + 20, weapon2.getWeaponValue());
        assertEquals(value_weapon3 + 5, weapon3.getWeaponValue());
        assertEquals(value_weapon4 + 10, weapon4.getWeaponValue());
        assertEquals(value_weapon5 + 15, weapon5.getWeaponValue());
    }

}
