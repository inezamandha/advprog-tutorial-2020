package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Nina", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Merchant", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member nina = new OrdinaryMember("Nina", "Merchant");
        ArrayList<Member> memberList = new ArrayList<>();
        member.addChildMember(nina);
        assertEquals(memberList, member.getChildMembers());
        member.removeChildMember(nina);
        assertEquals(memberList, member.getChildMembers());
    }
}
