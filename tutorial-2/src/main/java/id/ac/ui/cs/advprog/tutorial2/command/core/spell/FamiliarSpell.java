package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public abstract class FamiliarSpell implements Spell {
    protected Familiar familiar;
    // TODO: Complete Me

    // ikutin dari buku halaman 203 yang ada dirinya sendiri
    public FamiliarSpell(Familiar familiar){
        this.familiar = familiar;
    }

    @Override
    public void undo() {
        if (familiar.getPrevState() == FamiliarState.ACTIVE) {
            familiar.summon();
        }
        // TODO: Complete Me
        // di FamiliarState ada FamiliarState.SEALED, terus di familiarnya selain summon (yg current statenya active) ada sealed
        else if (familiar.getPrevState() == FamiliarState.SEALED) {
            familiar.seal();
        }
    }
}
