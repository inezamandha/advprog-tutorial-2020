package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        // ambil spell yang terbaru terus di cast jadi semacam kayak broadcast dan dia belum di undo
    }

    public void undoSpell() {
        // TODO: Complete Me
        // kalo belom di undo, di undo ambil ada di Spell
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
