package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.armory = armory;
    }

    @Override
    public void prepare() {
        this.setName("Metal Cluster Knight");
        this.armor = this.armory.craftArmor();
        this.skill = this.armory.learnSkill();
        // TODO complete me
    }
}
