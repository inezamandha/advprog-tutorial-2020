package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.armory = armory;
    }

    @Override
    public void prepare() {
        this.setName("Synthetic Knight");
        this.weapon = this.armory.craftWeapon();
        this.skill = this.armory.learnSkill();
        // TODO complete me
    }
}
